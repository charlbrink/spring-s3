# Getting Started

## Prerequisites
Requires AWS Account access and S3 bucket and object.
Below shows policy for bucket bucket700118

Create object (upload file) in bucket bucket700118 with name test-public-key.pem

Requires correct policy to allow access to the file, e.g.
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AllowAccessToOwner",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::686678429371:root"
            },
            "Action": "s3:*",
            "Resource": [
                "arn:aws:s3:::bucket700118",
                "arn:aws:s3:::bucket700118/*"
            ]
        }
    ]
}
```

Create parameter in AWS Parameter Store with Name below and any value
```
/config/application/spring-s3/var1
/config/application_awsdev/spring-s3/var1
/config/spring-s3/spring-s3/var1
/config/spring-s3_awsdev/spring-s3/var1
```

## Running
Set following environment variables:
AWS_ACCESS_KEY_ID=XXXXXXXX;AWS_SECRET_ACCESS_KEY=XXXXXXXXXX;AWS_REGION=us-west-2

Run with spring profile awsdev

##  API

### ResourceLoader
Reads file configured from either file/classpath/S3

```
curl http://localhost:8080/api/file/file-file
curl http://localhost:8080/api/file/classpath-file
curl http://localhost:8080/api/file/s3-file
```

### Configuration

Reads parameters from AWS Parameter Store
```
curl http://localhost:8080/api/parameter```
