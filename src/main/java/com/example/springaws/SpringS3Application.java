package com.example.springaws;

import com.example.springaws.config.SpringS3Properties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties(SpringS3Properties.class)
@SpringBootApplication
public class SpringS3Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringS3Application.class, args);
    }

}
