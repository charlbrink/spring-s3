package com.example.springaws.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

@Data
@ConfigurationProperties(prefix = "spring-s3")
public class SpringS3Properties {
    private Map<String, String> s3FileAlias;
    private String var1;
}
