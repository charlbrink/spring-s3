package com.example.springaws.web;

import com.example.springaws.config.SpringS3Properties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(value = "api", produces = {MediaType.TEXT_PLAIN_VALUE})
public class ApiController {
    private final SpringS3Properties springAwsProperties;
    private final ResourceLoader resourceLoader;

    @GetMapping("/file/{filename}")
    public ResponseEntity<String> readFile(@PathVariable(name = "filename") final String filename) throws IOException {
        String contents = readFileToString(springAwsProperties.getS3FileAlias().get(filename));
        return ResponseEntity.ok(contents);
    }

    @GetMapping("/parameter")
    public ResponseEntity<String> readFile() {
        return ResponseEntity.ok(springAwsProperties.getVar1());
    }

    private String readFileToString(final String path) throws IOException {
        Resource resource = resourceLoader.getResource(path);
        try (Reader reader = new InputStreamReader(resource.getInputStream(), StandardCharsets.UTF_8)) {
            return FileCopyUtils.copyToString(reader);
        }
    }

}
